# MIPS-exercises

Four small programs written in MIPS assembly. They have been tested under the
QtSpim emulator.
In no particular order, the programs are:

## Union and intersection

The user is asked how many arrays to enter, with a minumum of 1 and a maximum of 5.
Every array should be an array of integers. Press 0 to end insertion. At most
50 elements per array may be inserted.
After this, the user can choose whether to perform union of the arrays,
intersection or both. The user is then shown the result.

## String translation

The user is asked to enter a string. All "words", that is, substrings
surrounded by spaces, must be translated according to the following
rules:

+ "uno" -> "1"
+ "due" -> "2"
+ "tre" -> "3"
+ "quattro -> "4"
+ "cinque" -> "5"
+ "sei" -> "6"
+ "sette" -> "7"
+ "otto" -> "8"
+ "nove" -> "9"
+ any other string -> "?"

The user is then shown the result of the translation.


## Binary to natural

The user is asked to enter a binary number, using at most 10 digits.
Pressing 'x' will end insertion. The result of the conversion is then
shown to the user, implementing a recursive procedure.

## List splitter

The user is shown a menu with four options:
+Enter a new list
+Split all currently inserted lists in two
+Split all currently inserted lists in three
+Exit the program

In case options 2 and 3 are chosen, all lists are split and shown to the
user.
