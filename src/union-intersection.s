#Author:
#Cosimo Agati <cosimo.agati@stud.unifi.it>

.data

pos:	.space 24
first:	.asciiz "Welcome! This program will compute the union and intersection of up to 5 different arrays.\n\n"
msg:	.asciiz "How many arrays from 1 to 5 do you want to enter? "
next:	.asciiz "\nNow initializing the array number-"
nwln:	.asciiz ": "
msg1:	.asciiz "\n\nWhat do you want to do?\n
		1 - Union of arrays
		2 - Intersection of arrays
		3 - Union and Intersection of arrays\n
I choose: "
v1:	.asciiz "Vet-"
v2:	.asciiz ": ["
vu:	.asciiz "Vettore-Unione: ["
vi:	.asciiz "Vettore-Intersezione: ["
nwln1:	.asciiz "]\n"
wrg:	.asciiz "\n\nThe number you inserted is not valid!\n"

.text
.globl main
##############################################################Beginning Block#####################################
main:	
	addi $sp, $sp, -12	#saving in the stack frame $s registers, so that they can be used
	sw $s0, 0($sp)
	sw $s1, 4($sp)
	sw $s2, 8($sp)

	li $t0, 1		#initializing array counter to 0
	la $s0, pos		#saving the "pos" array in $s0

	la $a0, first		#printing "welcome" message
	li $v0, 4
	syscall

choose:	
	la $t7, choose		#saving the address to GOTO in case of wrong insertion
	la $a0, msg		#asking how many arrays to enter
	syscall

	li $v0, 5
	syscall

	bgt $v0, 5, wrong 
	blt $v0, 1, wrong
	move $t9, $v0		#saving in $t9 the number of arrays to create
#############################################################Insertion Block###########################################
loc:	
	la $a0, next		#telling the user which array is being allocated
	li $v0, 4
	syscall

	move $a0, $t0
	li $v0, 1
	syscall

	la $a0, nwln
	li $v0, 4
	syscall

	li $a0, 51
	li $v0, 9		#storing memory for a new array	through a sbrk syscall
	syscall

	addi $t1, $t0, -1
	sll $t1, $t1, 2
	add $t1, $s0, $t1	#calculating the position where to save the address of new array

	sw $v0, 0($t1)		#storing the address in "pos" array
	li $t1, 0		#initializing element counter to 0
	move $t2, $v0		#copying in $t2 the address of the current array
	
read:	
	li $v0, 12		#reading character
	syscall
	bne $v0, '0', store	#if char != 0, store it in the array

	beq $t0, $t9, askNext
	addi $t0, $t0, 1
	j loc

store:	
	move $t3, $v0
	li $a0, ' '
	li $v0, 11
	syscall			#printing a space

	sb $t3, 0($t2)		#storing character
	addi $t2, $t2, 1
	addi $t1, $t1, 1	#adding 1 to the counters

	blt $t1, 50, read	#if $t1 has not reached 50, read a new character
	beq $t0, $t9, askNext	#if every array has been allocated, ask the user what to do
	addi $t0, $t0, 1
	j read

askNext:
	la $t7, askNext		#saving the address to GOTO in case of wrong insertion
	la $a0, msg1		#asking the user what to do
	li $v0, 4
	syscall

	li $v0, 5
	syscall

	blt $v0, 1, wrong
	bgt $v0, 3, wrong
	move $t9, $v0		#storing user's choice in $t8

	move $t0, $s0
	beq $t9, 2, intersectionCheck
###############################################################Union Block###################################################
unionPrep:
	lw $t2, 0($t0)		#getting address of the first array
	lbu $t3, 0($t2)		#getting first element of the first array

	li $a0, 51		#allocating space for union array
	li $v0, 9
	syscall

	move $s1, $v0		#saving in $s1 the address of the union array
	move $t1, $s1		#copying it in $t1
	move $t5, $zero		#this counter is to prevent going beyond 50 positions

unionLoop:
	lbu $t4, 0($t1)
	beq $t3, $t4, nextUnionPosition
	beq $t4, $zero, unionInsertion

	addi $t1, $t1, 1
	j unionLoop

unionInsertion:
	sb $t3, 0($t1)			#storing the element in the union array
	addi $t5, $t5, 1
	beq $t5, 50, nextAction

nextUnionPosition:
	addi $t2, $t2, 1		#going to the next position of the array to be compared
	lbu $t3, 0($t2)
	move $t1, $s1			#restoring union array counter to 0
	beq $t3, $zero, nextUnionArray

	j unionLoop

nextUnionArray:
	addi $t0, $t0, 4		#going to the next word of the pos array
	lw $t2, 0($t0)
	beq $t2, $zero, nextAction

	lbu $t3, 0($t2)			#getting first element of the next array
	move $t1, $s1
	j unionLoop
############################################################Intersection Block################################################
nextAction:
	beq $t9, 1, print		#if the user wants to do union only, start printing arrays

intersectionCheck:			#in this loop we check if there is at least one empty array
	lw $t1, 0($t0)			#if there is one, the intersection array will surely be empty
	beq $t1, $zero, intersection	#if all arrays are checked, allocate intersection
	lbu $t2, 0($t1)

	beq $t2, $zero, emptyInsertion	#if an empty array is found, the intersection is empty
	addi $t0, $t0, 4
	j intersectionCheck

intersection:
	li $a0, 51			#allocating space for the intersection array
	li $v0, 9
	syscall

	move $t0 $s0			#copying in $t0 the address of the pos array
	lw $t2, 0($t0)			#getting address of the first array, which will be the base for comparison
	lbu $t3, 0($t2)			#getting first element of the first array

	move $s2, $v0			#copying in $s2 the address of the intersection array
	move $t1, $s2			#initializing position counter
	move $t8, $s2			#this counter is to store elements in the correct position

nextIntersection:
	addi $t0, $t0, 4
	lw $t6, 0($t0)			#getting address of the next array to compare
	beq $t6, $zero, finalComp	#if every array has been compared, check if the element is already in the array
	lbu $t7, 0($t6)			#getting first element of the array to compare

intersectionLoop:
	beq $t3, $t7, nextIntersection	#if the element is in the other array, compare next array
	addi $t6, $t6, 1		#going to the next position of the current array
	lbu $t7, 0($t6)			#getting element
	beq $t7, $zero, nextIntEl	#if the element is not found, check the next one

	j intersectionLoop

finalComp:
	lbu $t7, 0($t1)
	beq $t7, $zero, insPut		#if the end of intersection array is reached, put element
	beq $t3, $t7, nextIntEl		#if the element is already in the intersection, compare the next one
	addi $t1, $t1, 1		#going to the next position of the intersection array

	j finalComp

nextIntEl:
	addi $t2, $t2, 1
	lbu $t3, 0($t2)			#getting element
	beq $t3, $zero, print		#if the base array is finished, start printing

	move $t0, $s0			#copying in $t0 the address of the pos arrays
	addi $t0, $t0, 4
	lw $t6, 0($t0)			#getting address of the first array to be compared
	
	move $t1, $s2			#restoring position counter to $s2
	beq $t6, $zero, finalComp
	lbu $t7, 0($t6)			#getting first element

	j intersectionLoop

insPut:
	sb $t3, 0($t8)			#storing element in the array
	addi $t8, $t8, 1
	move $t1, $s2			#restoring position counter to $s2

	j nextIntEl

emptyInsertion:
	li $a0, 1
	li $v0, 9
	syscall
	move $s2, $v0
############################################################Print Block####################################################
print:	
	move $t0, $s0			#copying in $t0 the address of pos array
	lw $t2, 0($t0)			#getting address of the first array
	lbu $t3, 0($t2)			#getting element
	li $t1, 1			#saving in $t1 the index of the array to print

newArray:
	la $a0, v1			#printing Vet-i: [
	li $v0, 4
	syscall

	move $a0, $t1
	li $v0, 1
	syscall

	la $a0, v2
	li $v0, 4
	syscall

	move $a0, $t3
	li $v0, 11

printLoop:
	syscall				#printing character
	addi $t2, $t2, 1
	lbu $t3, 0($t2)

	beq $t3, $zero, nextPrint
	li $a0, ' '			#printing space
	syscall

	move $a0, $t3
	j printLoop

nextPrint:
	la $a0, nwln1			#printing a newline
	li $v0, 4
	syscall

	lw $t2, 0($t0)
	beq $t2, $s2, exit

	addi $t0, $t0, 4
	lw $t2, 0($t0)			#getting address of the next array to print
	beq $t2, $zero, uniOrIns

	beq $t2, $s2, printInt
	li $v0, 11

	lbu $t3, 0($t2)			#getting first element of the array to print
	addi $t1, $t1, 1
	j newArray

uniOrIns:
	beq $t9, 2, printInt		#if user wants to do intersection only
	move $t2, $s1			#print intersection, else, print union

printUnion:
	la $a0, vu			#printing "Vettore-intersezione"
	syscall

	sw $s2, 4($t0)			#storing intersection address in the pos array
	lbu $a0, 0($s1)

	beq $a0, $zero, nextPrint	#if the array is empty, print next array

	li $v0, 11
	j printLoop

printInt:
	la $a0, vi			#printing "Vettore-intersezione"
	syscall
	lbu $a0, 0($s2)
	
	move $t2, $s2
	sw $s2, 0($t0)

	beq $a0, $zero, nextPrint	#if the array is empty, print next array
	li $v0, 11
	j printLoop
####################################################################Exit##############################################################
exit:
	lw $s0, 0($sp)			#restoring $s registers and exiting the program
	lw $s1, 4($sp)
	lw $s2, 8($sp)

	addi $sp, $sp, 12
	li $v0, 10
	syscall

wrong:	
	la $a0, wrg			#if a wrong number is entered, ask again
	li $v0, 4
	syscall

	jr $t7
