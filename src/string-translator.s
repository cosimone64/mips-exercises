#Author:
#Cosimo Agati <cosimo.agati@stud.unifi.it>

.data

buffer:	.space 101
tra:	.asciiz "\n Risultato della traduzione:"
msg:	.asciiz "\n Welcome! This program will translate each separate subsequence of a string"
prompt: .asciiz "\n Insert the string to be translated: "

.text
.globl main

main:	
	la $a0, msg		#the program starts
	li $v0, 4
	syscall

read:	
	la $a0, prompt		#printing prompt
	syscall

store:	
	la $a0, buffer		#reading string and storing its address in $a0
	li $a1, 101
	li $v0, 8
	syscall

	move $t1, $a0		#using $t1 to compare each character in the string
	la $a0, tra
	li $v0, 4
	syscall
	
	li $v0, 11		#saving 11 in $v0 to print characters with syscalls
	lbu $t0, 0($t1)
	
compare:	
	beq $t0, $zero, exit	#checking which label to GOTO based on the first character
	beq $t0, 10, exit
	beq $t0, 'u', uno
	beq $t0, 'd', due

	beq $t0, 't', tre
	beq $t0, 'q', quat
	beq $t0, 'c', cinq

	beq $t0, 's', sxOst
	beq $t0, 'o' ott
	beq $t0, 'n', nov
	
invalidSeq:
	li $a0, ' '
	syscall
	li $a0, '?'			#printing '?' since an invalid sequence was found
	syscall

checkEnd:
	addi $t1, $t1, 1		#going to the next character of the string
	lbu $t0, 0($t1)
	beq $t0, $zero, exit		#if null or line feed is reached, exit
	beq $t0, 10, exit

invalidLoop:
	addi $t1, $t1, 1		#in this loop, we scan the string until
	lbu $t0 0($t1)			#we find a space or the string ends
	beq $t0, $zero, exit

	beq $t0, 10, exit
	beq $t0, ' ', scanSpaces
	j invalidLoop

scanSpaces:
	addi $t1, $t1, 1
	lbu $t0, 0($t1)
	bne $t0, ' ', compare
	j scanSpaces

uno:
	addi $t1, $t1, 1		#characters are compared one by one
	lbu $t0, 0($t1)
	bne $t0, 'n', invalidSeq

	addi $t1, $t1, 1
	lbu $t0, 0($t1)
	bne $t0, 'o', invalidSeq

	addi $t1, $t1, 1
	lbu $t0, 0($t1)
	li $t2, '1'

	beq $t0, ' ', printChar		#if a space is found or the string ends, print element
	beq $t0, $zero, printChar
	beq $t0, 10, printChar

	j invalidSeq

due:	
	addi $t1, $t1, 1		#characters are compared one by one
	lbu $t0, 0($t1)
	bne $t0, 'u', invalidSeq

	addi $t1, $t1, 1
	lbu $t0, 0($t1)
	bne $t0, 'e', invalidSeq

	addi $t1, $t1, 1
	lbu $t0, 0($t1)
	li $t2, '2'

	beq $t0, ' ', printChar		#if a space is found or the string ends, print element
	beq $t0, $zero, printChar
	beq $t0, 10, printChar

	j invalidSeq

tre:	addi $t1, $t1, 1		#characters are compared one by one
	lbu $t0, 0($t1)
	bne $t0, 'r', invalidSeq
	
	addi $t1, $t1, 1
	lbu $t0, 0($t1)
	bne $t0, 'e', invalidSeq

	addi $t1, $t1, 1
	lbu $t0, 0($t1)
	li $t2, '3'
	
	beq  $t0, ' ', printChar	#if a space is found or the string ends, print element
	beq $t0, $zero, printChar
	beq $t0, 10, printChar

	j invalidSeq 

quat:	addi $t1, $t1, 1
	lbu $t0, 0($t1)
	bne $t0, 'u', invalidSeq

	addi $t1, $t1, 1
	lbu $t0, 0($t1)
	bne $t0, 'a', invalidSeq

	addi $t1, $t1, 1
	lbu $t0, 0($t1)
	bne $t0, 't', invalidSeq

	addi $t1, $t1, 1
	lbu $t0, 0($t1)
	bne $t0, 't', invalidSeq

	addi $t1, $t1, 1
	lbu $t0, 0($t1)
	bne $t0, 'r', invalidSeq

	addi $t1, $t1, 1
	lbu $t0, 0($t1)
	bne $t0, 'o', invalidSeq

	addi $t1, $t1, 1
	lbu $t0, 0($t1)
	li $t2, '4'
	
	beq  $t0, ' ', printChar	#if a space is found or the string ends, print element
	beq $t0, $zero, printChar
	beq $t0, 10, printChar

	j invalidSeq

cinq:	addi $t1, $t1, 1
	lbu $t0, 0($t1)
	bne $t0, 'i', invalidSeq

	addi $t1, $t1, 1
	lbu $t0, 0($t1)
	bne $t0, 'n', invalidSeq

	addi $t1, $t1, 1
	lbu $t0, 0($t1)
	bne $t0, 'q', invalidSeq

	addi $t1, $t1, 1
	lbu $t0, 0($t1)
	bne $t0, 'u', invalidSeq

	addi $t1, $t1, 1
	lbu $t0, 0($t1)
	bne $t0, 'e', invalidSeq

	addi $t1, $t1, 1
	lbu $t0, 0($t1)
	li $t2, '5'

	beq  $t0, ' ', printChar	#if a space is found or the string ends, print element
	beq $t0, $zero, printChar
	beq $t0, 10, printChar

	j invalidSeq

sxOst:	addi $t1, $t1, 1
	lbu $t0, 0($t1)
	bne $t0, 'e', invalidSeq

	addi $t1, $t1, 1
	lbu $t0, 0($t1)

	beq  $t0, 'i', sei
	bne $t0, 't', invalidSeq
	
	addi $t1, $t1, 1
	lbu $t0, 0($t1)
	bne $t0, 't', invalidSeq

	addi $t1, $t1, 1
	lbu $t0, 0($t1)
	bne $t0, 'e', invalidSeq

	addi $t1, $t1, 1
	lbu $t0, 0($t1)
	li $t2, '7'
	
	beq  $t0, ' ', printChar	#if a space is found or the string ends, print element
	beq $t0, $zero, printChar
	beq $t0, 10, printChar

	j invalidSeq

sei:	addi $t1, $t1, 1
	lbu $t0, 0($t1)
	li $t2, '6'

	beq $t0, ' ', printChar		#if a space is found or the string ends, print element
	beq $t0, $zero, printChar
	beq $t0, 10, printChar
	
	j invalidSeq

ott:	addi $t1, $t1, 1
	lbu $t0, 0($t1)
	bne $t0, 't', invalidSeq

	addi $t1, $t1, 1
	lbu $t0, 0($t1)
	bne $t0, 't', invalidSeq

	addi $t1, $t1, 1
	lbu $t0, 0($t1)
	bne $t0, 'o', invalidSeq
	
	addi $t1, $t1, 1
	lbu $t0, 0($t1)
	li $t2, '8'

	beq $t0, ' ', printChar		#if a space is found or the string ends, print element
	beq $t0, $zero, printChar
	beq $t0, 10, printChar
	
	j invalidSeq
	
nov:	addi $t1, $t1, 1
	lbu $t0, 0($t1)
	bne $t0, 'o', invalidSeq

	addi $t1, $t1, 1
	lbu $t0, 0($t1)
	bne $t0, 'v', invalidSeq

	addi $t1, $t1, 1
	lbu $t0, 0($t1)
	bne $t0, 'e', invalidSeq
	
	addi $t1, $t1, 1
	lbu $t0, 0($t1)
	li $t2 '9'

	beq $t0, ' ', printChar		#if a space is found or the string ends, print element	
	beq $t0, $zero, printChar
	beq $t0, 10, printChar
	
	j invalidSeq

printChar:
	li $a0, ' '			#printing current character
	syscall

	move $a0, $t2			
	syscall
	j scanSpaces

exit:					#exiting the program
	li $v0, 10
	syscall	
