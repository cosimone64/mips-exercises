#Author:
#Cosimo Agati <cosimo.agati@stud.unifi.it>

.data

bNum:	.space 11
start:	.asciiz "\n Welcome! This program will convert a pure binary number in a decimal number: "
prompt:	.asciiz "\n Please insert a maximum of 10 digits. 'x' to end the number.\n         "
err:	.asciiz "\n Insert a valid number! (previous digts are still valid)\n         " 
msg1:	.asciiz "\n Binario puro: "
msg2:	.asciiz "\n Valore decimale corrispondente: "
noNum:	.asciiz "\n No number inserted!"

.text
.globl main

main:
	la $t0, bNum		#storing in $t0 the address of the array
	move $t2, $t0		#copying it in $t2 to check later if the array is empty after insertion

	la $a0, start		#the program begins
	li $v0, 4
	syscall

	la $a0, prompt		#printing prompt
	li $v0, 4
	syscall

	li $t1, 0
	
read:	
	li $v0, 12		#read character
	syscall

	beq $v0, '0', arrIns	#if '0' is inserted, put it in the array
	beq $v0, '1', arrIns	#if '1' is inserted, put it in the array
	beq $v0, 'x', prep	#if 'x' is inserted, the array is finished

wrong:	
	la $a0, err		#if a wrong character is inserted, ask again
	li $v0, 4
	syscall

	j read

arrIns:	
	sb $v0, 0($t0)		#storing character in the array
	addi $t0, $t0, 1	#going to the next position of the array
	addi $t1, $t1, 1
	beq $t1, 10, prep	#if 10 elements are inserted, start converting

	j read

prep:	
	beq $t0, $t2, noNumber	#if no number was typed, print a message and exit
	la $a0, bNum		#passing the address of V to Conv
	addi $a1, $t1, -1	#passing m to Conv
	li $a2, 0		#passing k to Conv

	li $v0, 0		#initializing total return value to 0
	addi $sp, $sp, -8	#saving return address and array address in the stack
	sw $ra, 0($sp)
	sw $a0, 4($sp)

	jal Conv		#call Conv(V, m, k)

result:	
	lw $a0, 4($sp)		#restoring return address and array address from the stack
	lw $ra, 0($sp)
	addi $sp, $sp, 8
	move $t9, $v0		#copying the result in $t9

	move $t0, $a0		#copying array address in $t0
	la $a0, msg1
	li $v0, 4
	syscall

bin:
	lbu $t1, 0($t0)		#printing binary number
	beq $t1, $zero, dec	#if the array is finished, print final result

	move $a0, $t1
	li $v0, 11
	syscall

	addi $t0, $t0, 1	#adding 1 to the counter
	j bin
	
dec:
	la $a0, msg2		#printing message
	li $v0, 4
	syscall

	move $a0, $t9		#printing final result
	li $v0, 1
	syscall

	j exit

noNumber:
	la $a0, noNum		#if no number was inserted, tell the user
	li $v0, 4
	syscall
exit:
	li $v0, 10		#exit
	syscall

Conv:
	add $t1, $a0, $a2	#adding k to the address of the array
	lbu $t0, 0($t1)
	addi $t0, $t0, -48	#this is V[k], obtained by subtracting the ascii code from the character
	beq $a1, $a2, RetVk	#if m == k, return V[k]

	addi $sp, $sp, -16

	sw $ra, 0($sp)		#storing return address, array address,
	sw $a0, 4($sp)		#m and k in the stack to call Mul2
	sw $a1, 8($sp)
	sw $a2, 12($sp)

	move $a0, $t0		#passing V[k] to Mul2
	sub $a1, $a1, $a2	#passing m-k to Mul2
	jal Mul2		#call Mul2(V[k], m-k)

	lw $a2, 12($sp)		#retrieving values from the stack
	lw $a1, 8($sp)
	lw $a0, 4($sp)
	lw $ra, 0($sp)

	addi $sp, $sp, 16
	add $v0, $v0, $v1	#adding to the total return value the value returned from Mul2
	addi $a2, $a2, 1	#adding 1 to k

	addi $sp, $sp, -4	#storing return address in the stack
	sw $ra, 0($sp)
	jal Conv		#call Conv with (V, m, k+1)

	lw $ra, 0($sp)		#retrieving return address from the stack
	addi $sp, $sp, 4
	jr $ra

RetVk:
	add $v0, $v0, $t0	#return V[k]
	jr $ra

Mul2:	
	beq $a0, $zero, retZr	#if V[k] == 0, return 0
	bne $a1, 1, Mul2Rec	#if b != 1, start recursive call
	sll $v1, $a0, 1		#return 2*V[k]

	jr $ra

retZr:	
	li $v1, 0		#return 0
	jr $ra

Mul2Rec:	
	sll $a0, $a0, 1		#call Mul2 with 2a, b-1
	addi $a1, $a1, -1	#subtracting 1 to m-k

	addi $sp, $sp, -4	#storing return address in the stack
	sw $ra, 0($sp)
	jal Mul2		#call Mul2(2*V[k], m-k-1)

	lw $ra, 0($sp)		#retrieving return value from the stack
	addi $sp, $sp, 4
	jr $ra
