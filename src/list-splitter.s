#Autor:
#Cosimo Agati <cosimo.agati@stud.unifi.it>

.data

new:	.asciiz "Insert a new list: (press 0 to stop)\n"
nex:	.asciiz "Next number: "
ex:	.asciiz "\nThank you for using this program!"
choose: .asciiz "\nSelect your option:\n
		 1 - Insert a new list\n
		 2 - Split in two all current lists\n
		 3 - Split in three all current lists\n
		 4 - Exit the program\n
I choose: "
err:    .asciiz "Insert a valid number!\n"
err1:	.asciiz "\nThere are no lists to split! Create one first.\n"
sofar:	.asciiz "\nLists created so far:\n\n"
after:	.asciiz "\nLists after split:\n\n"
noeff:	.asciiz "The split has no effect on this list, since it is empty\n\n"
list:	.asciiz "List-"
bg:	.asciiz ": ["
nwln:	.asciiz "]\n"
nwln2:	.asciiz "\n"
nil:	.asciiz "nil"
jtable: .word opt1
        .word checkSplit
        .word checkSplit
        .word opt4

.text
.globl main

main:	
	addi $sp, $sp, -20		#saving $s registers in the stack, so that they can be used
	sw $s0, 0($sp)
	sw $s1, 4($sp)
	sw $s2, 8($sp)
	sw $s3, 12($sp)
	sw $s4, 16($sp)

	move $s1, $sp			#copying in $s1 the address of $sp before any list is created
	li $s3, 0			#initializing 'i' counter to 0
	la $s0, jtable			#loading in $s0 the address of the jump table

ask:
	la $a0, choose			#printing message to ask the user what to do
	li $v0, 4
	syscall

chs:
	li $v0, 5			#reading user input
	syscall	


	blt $v0, 1, wrong		#if $v0 < 1, GOTO chs
	bgt $v0, 4, wrong		#if $v0 > 4, GOTO chs
	addi $v0, $v0, -1		#subtracting 1 to $v0 and multiplying to 4
	sll $t0, $v0, 2

	add $t1, $s0, $t0		#adding the offset to the jump table address ($s0)
	lw $t1, 0($t1)
	jr $t1

wrong:
	la $a0, err			#if a wrong number is entered, ask again
	li $v0, 4
	syscall
	
	j chs

opt1:
	la $a0, new			#telling the user to insert a new list
	li $v0, 4
	syscall

	move $t8, $zero			#initializing head and tail to 0
	move $t9, $zero
	li $t7, 0			#initializing element counter to 0
	addi $s3, $s3, 1		#adding 1 to "i", the name of the list to print
	j next

checkSplit:
	beq $s1, $sp, noLists		#if no list is stored, tell the user
	la $a0, after
	li $v0, 4

	syscall
	move $s4, $s1			#initializing the counter to get elements
	
	move $s2, $sp			#saving in $s2 the stack limit before the split
	beq $t0, 4, opt2		#start split2 or split3 based on user choice
	beq $t0, 8, opt3

opt2:
	lw $a0, -8($s4)			#getting number of elements of the list to split
	lw $a1, -12($s4)		#getting head of the list to split
	addi $sp, $sp, -4
	sw $ra, 0($sp)			#storing return address in the stack

	jal split2			#call split2
	lw $ra, 0($sp)			#restoring return value
	addi $sp, $sp, 4

	addi $s4, $s4, -12		#going to the next three  words of the stack
	bne $s4, $s2, opt2		#if there are still lists to split, continue loop
	move $s4, $s1			#else, start printing lists
	j allLists
	
opt3:	
	lw $a0, -8($s4)			#getting number of elements of the list to split
	lw $a1, -12($s4)		#getting head of the list to split
	addi $sp, $sp, -4
	sw $ra, 0($sp)			#storing return address

	jal split3			#call split3
	lw $ra, 0($sp)			#restoring return address
	addi $sp, $sp, 4

	addi $s4, $s4, -12		#going to the next three words of the stack
	bne $s4, $s2, opt3		#if there are still lists to split, continue loop
	move $s4, $s1			#else, start printing lists

	j allLists

opt4:
	beq $sp, $s1, exit		#if the stack is fueled, restore s registers and exit
	addi $sp, $sp, 12		#else, de-allocate 3 words in the stack
	j opt4

exit:
	lw $s4, 16($sp)			#restoring s registers and de-allocating the stack
	lw $s3, 12($sp)
	lw $s2, 8($sp)

	lw $s1, 4($sp)
	lw $s0, 0($sp)
	addi $sp, $sp, 20

	la $a0, ex			#printing exit message
	li $v0, 4
	syscall	

	li $v0, 10			#exit
	syscall	
#######################################################Split-2 and Split-3 shared Block###########################3
noLists:
	la $a0, err1			#if a split is called without any list, print error message
	li $v0, 4
	syscall

	j ask				#ask next option

emptyList:
	lw $a0, -4($s4)			#loading index of the list to print
	addi $sp, $sp, -4
	sw $ra, 0($sp)			#stoing return address

	jal print			#call print
	lw $ra, 0($sp)			#restoring return address from the stack
	addi $sp, $sp, 4

	la $a0, noeff			#telling the user that the split has no effect on an empty list
	la $v0, 4
	syscall

	jr $ra
#########################################################Insertion Block####################################
next:	
	la $a0, nex			#asking the user to enter a new number
	li $v0, 4
	syscall

	li $v0, 5			#reading user input
	syscall

	beq $v0, $zero, alloc		#if $v0 == 0, start printing lists
	move $t0, $v0			#else, copy it in $t0 to put it in a list
	addi $t7, $t7, 1		#adding 1 to the counter

	li $a0, 12			#creating a new block of 12 bytes to store the new element
	li $v0, 9
	syscall

	sw $zero, 0($v0)		#initializing pointers to null
	sw $zero, 8($v0)
	sw $t0, 4($v0)
	bne $t8, $zero, last		#if head != 0, (list not empty) link the new element to the last one

	move $t8, $v0			#else, the first element is inserted, which means head = tail = $v0
	move $t9, $v0

	j next

last:
	sw $v0, 8($t9)			#the new element becomes the tail of the list
	sw $t9, 0($v0)
	move $t9, $v0

	j next

alloc:
	addi $sp, $sp, -12		#storing elements in the stack
	sw $t8, 0($sp)			#head
	sw $t7, 4($sp)			#number of elements
	sw $s3, 8($sp)			#index

	move $s4, $s1			#start printing lists

allLists:
	la $a0, sofar			#showing lists created so far to the user
	li $v0, 4
	syscall

loopLists:
	lw $a0, -4($s4)			#getting index of the list to print from the stack
	lw $a1, -12($s4)		#getting head of the list to print from the stack

	addi $sp, $sp, -4
	sw $ra, 0($sp)			#storing return value in the stack
	jal print			#call print

	lw $ra, 0($sp)			#retrieving return value
	addi $sp, $sp, 4

	addi $s4, $s4, -12		#going to the next 3 words of the stack
	beq $s4, $sp, ask		#after printing all lists, ask user what to do
	j loopLists

########################################################Split-2 Block##################################
split2:
	beq $a0, $zero, emptyList	#if the list to split is empty, the split has no effect
	beq $a0, 1, onlyOne		#if it has only one element, it is split in a different way
	
	div $t2, $a0, 2			#dividing number of elements by 2
	li $t1, 1			#initializing element counter to 1
	move $t3, $a1			#copying head in a temporary register

split2Loop:
	beq $t1, $t2, linkHalf		#if every element is loaded, link the new list
	lw $t3, 8($t3)			#else, go to the next element in the list
	addi $t1, $t1, 1		#adding 1 to the counter

	j split2Loop

linkHalf:
	lw $t5, 8($t3)			#saving in $t5 the head of the new list
	sw $zero, 8($t3)		#putting null pointer at the end of the first list
	sw $zero, 0($t5)		#putting null pointer at the beginning of the new list

	sub $t4, $a0, $t2		#calculating number of elements in the new list
	sw $t2, -8($s4)			#storing in the stack the new number of elements of split list

storeHalf:
	addi $s3, $s3, 1		#calculating index of new list
	lw $a0, -4($s4)			#getting index of the list to pring and passing it to print
	addi $sp, $sp, -12		#storing in the stack the elements of new list

	sw $t5, 4($sp)			#head
	sw $t4, 8($sp)			#number of elements
	sw $s3, 12($sp)			#index

	addi $sp, $sp, -4
	sw $ra, 0($sp)			#storing return value
	jal print			#call print
	lw $ra, 0($sp)			#retrieving return value from the stack

	addi $sp, $sp, 4
	move $a0, $s3			#passing index of new list
	move $a1, $t5			#passing head of the new list

	addi $sp, $sp, -4
	sw $ra, 0($sp)			#storing return value
	jal print			#call print
	lw $ra, 0($sp)			#retrieving return value from the stack

	addi $sp, $sp, 4		#saving elements in the stack
	la $a0, nwln2			#priting a newline
	la $v0, 4
	syscall

	jr $ra

onlyOne:
	sw $zero, -12($s4)		#storing null pointer at the head of the first list
	sw $zero, -8($s4)		#storing number 0 of elements

	li $t4, 1			#loading in $t4 the number of elements of the second list (1)
	move $t5, $a1			#copying in $t5 the head of the first list, which becomes head of the second list
	move $a1, $zero			#passing a null head to print
	j storeHalf
##############################################################Split-3 Block#####################################
split3:
	beq $a0, $zero, emptyList	#if the list is empty, the split has no effect
	beq $a0, 1, oneOrTwo		#if it has one or two elements, it is split in a different way
	beq $a0, 2, oneOrTwo

	move $t2, $a0			#copying number of elements in $t2
	div $a0, $a0, 3			#dividing number of elements by 3
	sw $a0, -8($s4)			#storing in the stack the new number of elements of split list
	sub $t7, $t2, $a0		#calculating the number of remaining elements

	li $t1, 1			#initializing element counter to 1
	addi $sp, $sp, -12
	sw $ra, 0($sp)			#storing in the stack values to be used after jal
	sw $a1, 4($sp)
	sw $t7, 8($sp)
	
	jal split3FirstLoop		#call split3FirstLoop
	lw $t7, 8($sp)			#retriving values
	lw $a1, 4($sp)
	lw $ra, 0($sp)

	addi $sp, $sp, 12

linkFirstThird:
	move $t5, $v0			#copying in $t5 the head of the new list
	lw $a0, -4($s4)			#passing index of the original list to print

printFirstThird:

	addi $sp, $sp, -8
	sw $ra, 0($sp)			#storing return value
	sw $t5, 4($sp)			#saving $t5 in the stack, since it will be used after jal

	jal print			#printing original third of the list
	lw $t5, 4($sp)			#this is to make sure that $t5 has the same value
	lw $ra, 0($sp)			#retrieving return value
	addi $sp, $sp, 8

	li $t1, 1			#re-initializing counter to 1
	div $a0, $t7, 3			#passing number of elements of new list
	move $t8, $a0			#copying in $t8 the number of elements of second third
	
	beq $t8, $zero, emptySecond	#if the second list is empty, it is split in a different way
	move $a1, $t5			#passing head of the list to split

	addi $sp, $sp, -12
	sw $ra, 0($sp)			#storing in the stack values to be used after jal
	sw $t5, 4($sp)
	sw $t8, 8($sp)

	jal split3FirstLoop		#call split3FirstLoop
	
	lw $t8, 8($sp)			#retrieving values
	lw $t5, 4($sp)
	lw $ra, 0($sp)
	addi $sp, $sp, 12

linkSecondThird:
	move $t4, $v0			#copying in $t4 the head of the final third
	sub $t3, $t7, $t8		#number of elements of the final list

printSecondThird:
	addi $s3, $s3, 1		#calculating index of the second third
	addi $sp, $sp, -12		#storing in the stack the elements of the second third

	sw $t5,	4($sp)			#head
	sw $t8, 8($sp)			#number of elements
	sw $s3, 12($sp)			#index

	move $a0, $s3			#passing index of the second third to print
	move $a1, $t5			#passing head of the second third to print
	addi $sp, $sp, -12

	sw $ra, 0($sp)
	sw $t3, 4($sp)			#saving head and number of elements of final third
	sw $t4, 8($sp)			#in the stack, since they will be used after jal
	jal print			#printing the second third of the list

	lw $t4, 8($sp)			#restoring values from the stack
	lw $t3, 4($sp)
	lw $ra, 0($sp)
	addi $sp, $sp, 12		#storing in the stack elements of the second third

printFinalThird:
	addi $s3, $s3, 1		#calculating index of the final third
	addi $sp, $sp, -12		#storing in the stack the elements of final third

	sw $t4, 4($sp)			#head
	sw $t3, 8($sp)			#number of elements
	sw $s3, 12($sp)			#index

	move $a0, $s3			#passing index of the final third to print
	move $a1, $t4			#passing head of the final third to print

	addi $sp, $sp, -4
	sw $ra, 0($sp)			#storing return address
	jal print			#call print

	lw $ra, 0($sp)			#retrieving return address
	addi $sp, $sp, 4

	la $a0, nwln2			#printing a newline
	li $v0, 4
	syscall
	jr $ra

emptySecond:
	move $t4, $t5			#the head of the second third becomes the head of the final third
	move $t5, $zero			#the head of the second third is null
	move $t3, $t7			#humber of elements of the second third becomes number of elements of final third

	j printSecondThird

split3FirstLoop:
	bne $t1, $a0, split3Loop	#if not enough elements are loaded, continue loop
	lw $v0, 8($a1)			#return head of the new list
	sw $zero, 0($v0)		#putting null pointer at the beginning of the new list
	sw $zero, 8($a1)		#putting null pointer at the end of the previous list

	jr $ra

split3Loop:
	lw $a1, 8($a1)			#going to the next element of the list
	addi $t1, $t1, 1
	j split3FirstLoop

oneOrTwo:
	move $t4, $a1			#the head of the list becomes the head of the final third
	move $a1, $zero			#the head of the original third is null, and is passed to print
	move $t3, $a0			#the number of elements of the original third becomes number of final third

	sw $zero, -12($s4)		#storing null element at the head of the first list
	sw $zero, -8($s4)		#storing number 0 of elements
	lw $a0, -4($s4)			#passing index of the original third to print

	addi $sp, $sp, -12
	sw $ra, 0($sp)			#storing $t3 and $t4 in the stack, since they will be used after jal
	sw $t3, 4($sp)
	sw $t4, 8($sp)

	jal print			#call print

	lw $t4, 8($sp)			#restoring values from the stack
	lw $t3, 4($sp)
	lw $ra, 0($sp)
	addi $sp, $sp, 12

	move $t5, $zero			#the head of the second third is null
	move $t8, $zero			#saving 0 as the number of elements of second third
	j printSecondThird
##############################################################Print Block####################################
print:	
	move $t1, $a0			#storing idex in a temporary register
	la $a0, list			#print "List-"
	li $v0, 4
	syscall

	move $a0, $t1			#retrieving index from $t1
	li $v0, 1			#print index
	syscall

	la $a0, bg			#print ": ["
	li $v0, 4
	syscall

	addi $sp, $sp, -4
	sw $ra, 0($sp)			#storing return value in the stack
	jal printEl			#call printEl

	lw $ra, 0($sp)			#retrieving return value
	addi $sp, $sp, 4

	la $a0, nwln			#printing a newline, since the list is complete
	li $v0, 4
	syscall

	jr $ra

printEl:
	beq $a1, $zero, printNil	#if the list is empty, print "nil"
	lw $a0, 4($a1)			#printing first element
	li $v0, 1

	syscall
	lw $a1, 8($a1)			#go to the next element

verifyLoop:
	bne $a1, $zero, printLoop	#if the element is not null, continue loop
	jr $ra

printLoop:
	li $a0, ' '			#printing space
	la $v0, 11
	syscall

	lw $a0, 4($a1)			#printing element
	li $v0, 1
	syscall

	lw $a1, 8($a1)			#go to the next element
	j verifyLoop

printNil:
	la $a0, nil			#print "nil"
	li $v0, 4
	syscall
	jr $ra
